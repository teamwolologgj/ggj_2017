﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class InputSound : MonoBehaviour {

    public delegate void OnSoundChecked(CalibrateInput.SoundLevel soundLevel);
    public static event OnSoundChecked OnSoundCheckedEvent;
    [SerializeField]
    private GameObject _recordTimer;
    private bool isRecording = false;
    private MicrophoneInput _mic;
    private CalibrateInput.SoundLevel resultingSoundLevel;
    private CalibrateInput _calib;
    Scene currentScene;

    private void Start()
    {
        _mic = FindObjectOfType<MicrophoneInput>();
        MicrophoneInput.OnRecordEndEvent += CheckRecording;
        currentScene = SceneManager.GetActiveScene();
        _calib = FindObjectOfType<CalibrateInput>();
    }
    private void OnDestroy()
    {
        MicrophoneInput.OnRecordEndEvent -= CheckRecording;
    }
    private void Update()
    {
        if (isRecording)
            return;
        // Monster level -> EnemySelect.selectedEnemy != null
        // Shop level -> bovenstaande niet
        // if ([...] && (EnemySelect.selectedEnemy != null || SHOPLEVEL))
        if (Input.GetKeyDown(KeyCode.Space) && ((currentScene.name == "Level1" || currentScene.name == "Level2" && EnemySelect.selectedEnemy != null) || currentScene.name == "Shop") && !isRecording)
        {
            _mic.StartRecording();
            _recordTimer.SetActive(true);
        }
    }
    private void CheckRecording(AudioSource source)
    {
        if (isRecording)
            return;
        isRecording = true;
        StartCoroutine(AudioSpectrumanalyzer.CalculateMedianFreq(source, (x) =>
        {
            isRecording = false;
            if (x < 70)
            {
                Debug.Log("Try again feedback AND you have to press space again!");
                StopAllCoroutines();
                return;
            }
            resultingSoundLevel = _calib.FindSoundLevel(x);
                OnSoundCheckedEvent(resultingSoundLevel);


        }
        ));
    }
}
