﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SplashScript : MonoBehaviour {
    
    public GameObject wololo;
    public GameObject ggj;
    public GameObject game;

    // Use this for initialization
	void Start ()
    {
        this.Invoke("NextScreen", 3);
        this.Invoke("NextNextScreen", 6);
        this.Invoke("PlayGame", 9);
	}
	
    void NextScreen()
    {
        this.ggj.SetActive(false);
        this.wololo.SetActive(true);
    }

    void NextNextScreen()
    {
        this.wololo.SetActive(false);
        this.game.SetActive(true);
    }

	// Update is called once per frame
	void PlayGame()
    {
        SceneManager.LoadScene("Calibration");
    }
}
