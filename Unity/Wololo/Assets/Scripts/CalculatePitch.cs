﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CalculatePitch : MonoBehaviour {

    // Must be a multitude of 2
    private int _spectrumLenght = 1024;
    /// <summary>
    /// Minimum amplitude to extract pitch
    /// </summary>
    private float _minimumAplituredThreshold = 0.00f;
    private float _outputSampleRate;

    private void Awake()
    {
        //Sample rate of the audio
        _outputSampleRate = AudioSettings.outputSampleRate;
    }
    private void Start()
    {
        MicrophoneInput.OnRecordEndEvent += OnRecordEnd;
    }
    private void OnDestroy()
    {
        MicrophoneInput.OnRecordEndEvent -= OnRecordEnd;
    }
    private void OnRecordEnd(AudioSource source)
    {
        Debug.Log("Outputsamplerate: " + _outputSampleRate);
        //Debug.Log(Calculate(source));
    }
    private float Calculate(AudioSource source)
    {
        Debug.Log(source.clip.name);
        source.Play();
        float[] spectrum = new float[_spectrumLenght];
        source.GetSpectrumData(spectrum, 1, FFTWindow.BlackmanHarris);
        //StartCoroutine(printData(spectrum));

        // spectrum
        float maxV = 0;
        int maxN = 0;
        for (int i = 0; i < _spectrumLenght; i++)
        { // find max
            if (spectrum[i] > maxV && spectrum[i] > _minimumAplituredThreshold)
            {
                maxV = spectrum[i];
                // maxN is the index of max
                maxN = i;
            }

        }
        float freqN = maxN;
        if (maxN > 0 && maxN < _spectrumLenght - 1)
        { // Interpolate index using neighbours
            var dL = spectrum[maxN - 1] / spectrum[maxN];
            var dR = spectrum[maxN + 1] / spectrum[maxN];
            freqN += 0.5f * (dR * dR - dL * dL);
        }
        // Returns Pitch Value
        return freqN * (_outputSampleRate / 2) / _spectrumLenght; 
    }
    private IEnumerator printData(float[] data)
    {
        foreach (var dat in data)
        {
            Debug.Log(dat);
            yield return new WaitForEndOfFrame();
        }
    }
}
