﻿using UnityEngine;

public class Stats : MonoBehaviour
{

    public int knockbackAmount;
    /// <summary>
    /// Amount of grids per movement
    /// </summary>
    public int movementAmount;

    public float health;
    public float maxHealth;
    public float curHealth;
    bool isHit;

    public GameObject healthBar;

    // Use this for initialization
    void Start()
    {

        curHealth = maxHealth;

    }
    void Update()
    {
        float calcHealth = curHealth / maxHealth;
        SetHealthBar(calcHealth);
    }


    //when the player gets damaged
    /*public void Damage () {
    //change 1f to the current damage of the player
    curHealth -= 1f;
    float calcHealth = curHealth / maxHealth;
    SetHealthBar (calcHealth);

    }*/

    public void SetHealthBar(float myHealth)
    {
        healthBar.transform.localScale = new Vector3(Mathf.Lerp(0f, 20.8f, myHealth), healthBar.transform.localScale.y, healthBar.transform.localScale.z);
    }





}
