﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RecordFailed : MonoBehaviour {

	void Start(){

		this.gameObject.SetActive (false);

	}
	// Use this for initialization
	void OnEnable () {

		//show error on screen met een press space to continue whatever


	}

	void OnGUI(){
		GUIStyle myStyle = new GUIStyle ();
		myStyle.fontSize = 48;
		myStyle.normal.textColor = Color.red;
		GUI.Label(new Rect(Screen.width/8 ,Screen.height/2 - 10,Screen.width/2, Screen.height),"Recording failed, press spacebar to try again", myStyle);
	}

	// Update is called once per frame
	void Update () {

		if (Input.GetKeyDown (KeyCode.Space)) {

			this.gameObject.SetActive(false);

		}

	}
}
