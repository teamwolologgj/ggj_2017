﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GridManager : MonoBehaviour {

	Vector2 gridSize;
	public GameObject Tile;
	GameObject[][] gridOfGameObjects;
	void Start()
	{
		gridSize = new Vector2 (3, 15);
		gridOfGameObjects = new GameObject[(int)gridSize.x][];

		for (int x = 0; x < gridSize.x; x++) {
		
			gridOfGameObjects[x] = new GameObject[(int)gridSize.y];

			for (int y = 0; y < gridSize.y; y++) {
			
				//GameObject go = GameObject.CreatePrimitive (PrimitiveType.Cube);
				//GameObject go = GameObject.
				//gridOfGameObjects[x][y] = go;
				Instantiate (Tile);
			
			}
		}
	}

}
