﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopCheck : MonoBehaviour
{
    public static int correctSound;
    public static GameObject selectedItem;

    void Start () {

        InputSound.OnSoundCheckedEvent += SoundCheck;
	}

    private void SoundCheck(CalibrateInput.SoundLevel soundLevel)
    {
        if ((int)soundLevel == correctSound)
        {
            if (selectedItem.name == "Sword")
                PlayerStats.damage++;
            if (selectedItem.name == "Fist")
                PlayerStats.knockBack++;
        }
    }
    
    void OnDestroy()
    {
        InputSound.OnSoundCheckedEvent -= SoundCheck;
    }
}
