﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopSelect : MonoBehaviour {

    public static GameObject selectedItem = null;
    public static CalibrateInput.SoundLevel correctSound;
    private CalibrateInput _calib;
    private CalibrateInput Input;

    private void Staet()
    {
        _calib = FindObjectOfType<CalibrateInput>();
    }
	public void Select ()
    {
        if (selectedItem)
            return;
        Debug.Log("Selected " + gameObject.name);
        selectedItem = this.gameObject;
        correctSound = (CalibrateInput.SoundLevel)Random.Range(0, 2);
        ShopCheck.correctSound = (int)correctSound;
        ShopCheck.selectedItem = this.gameObject;

        _calib.GetCalibratedSound(correctSound).loop = true;
        Invoke("StopAudio", 5);
        _calib.GetCalibratedSound(correctSound).Play();
	}
	
    private void StopAudio()
    {
        _calib.GetCalibratedSound(correctSound).Stop();
    }


}
