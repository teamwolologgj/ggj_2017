﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySelect : MonoBehaviour {

    public GameObject selectionObject;

    private void Start()
    {
        Turn.OnTurnEndEvent += DeSelect;
        _calib = FindObjectOfType<CalibrateInput>();
    }
    private void OnDestroy()
    {
        Turn.OnTurnEndEvent -= DeSelect;
    }
    private void OnLevelWasLoaded()
    {
        selectedEnemy = null;
    }
    public static GameObject selectedEnemy = null;
    public static CalibrateInput.SoundLevel correctSound;
    private CalibrateInput _calib;

	public void Select()
    {
        if (selectedEnemy)
            return;
        Debug.Log("Selected " + gameObject.name);
        selectedEnemy = this.gameObject;
        correctSound = (CalibrateInput.SoundLevel)Random.Range(0, 2);
        // TODO Nice animation of select
        _calib.GetCalibratedSound(correctSound).loop = true;
        Invoke("StopAudio", 5);
        _calib.GetCalibratedSound(correctSound).Play();
        this.selectionObject.SetActive(true);

        this.GetComponent<EnemyMultipleParticalSender>().Emit();
    }
    private void StopAudio()
    {
        _calib.GetCalibratedSound(correctSound).Stop();
    }
    public void DeSelect(bool isForced)
    {
        this.selectionObject.SetActive(false);
        selectedEnemy = null;
    }
    
}
