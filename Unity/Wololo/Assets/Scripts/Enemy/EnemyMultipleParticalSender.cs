﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMultipleParticalSender : MonoBehaviour
{
    private ParticleSystem[] particles;

	void OnEnable()
    {
        this.particles = this.GetComponentsInChildren<ParticleSystem>();
        for (int i = 0; i < this.particles.Length; i++)
        {
            this.particles[i].Stop();
        }
	}
	
	public void Emit()
    {
		if (this.particles.Length > 0)
        {
            for (int i = 0; i < this.particles.Length; i++)
            {
                particles[i].Play();
            }
        }
	}
}
