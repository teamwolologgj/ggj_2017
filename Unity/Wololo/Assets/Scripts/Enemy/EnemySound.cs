﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySound : MonoBehaviour {

    private Turn _turn;
    private PlayerAttack _playerAttack;
	private void Start()
    {
        _playerAttack = FindObjectOfType<PlayerAttack>();
        if (!_playerAttack)
            Debug.LogError("Could not Find PlayerAttack Script");
        _turn = FindObjectOfType<Turn>();
        if (!_turn)
            Debug.LogError("Could not find Turn Script");
        InputSound.OnSoundCheckedEvent += CheckSoundLevel;
    }
    private void OnDestroy()
    {
        InputSound.OnSoundCheckedEvent -= CheckSoundLevel;
    }
    public void CheckSoundLevel(CalibrateInput.SoundLevel soundLevel)
    {
        if (!EnemySelect.selectedEnemy)
            return;

        if (EnemySelect.selectedEnemy != this.gameObject)
            return;
        Debug.Log("CheckSoundLevel");
        if (soundLevel == EnemySelect.correctSound)
        {
            Debug.Log("Hit");
            // Correct
            _playerAttack.Hit();

        }
        else
        {
            Debug.Log("Miss");

            // Fail
            //_playerAttack.Hit();
        }

        _turn.TurnEnd(false);
    }
}
