﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof (Stats))]
public class EnemyMovement : MonoBehaviour {

    private Stats _stats;
    public const float DISTANTS_BETWEEEN_GRID = -1.38f;
    private const float WAIT_TIME_BETWEEN_STEPS = 0.75f;

    private void Start () {
        _stats = GetComponent<Stats>();
        Turn.OnTurnStartEvent += OnTurnStart;        
	}
    private void OnDestroy()
    {
        Turn.OnTurnStartEvent -= OnTurnStart;
    }
    private void OnTurnStart()
    {
        //Debug.Log(this.gameObject.name + " Movement Speed: " + _stats.movementAmount);
        StartCoroutine(MoveToNewLocation(_stats.movementAmount));
    }
    private IEnumerator MoveToNewLocation(int stepsToMove)
    {
        Debug.Log(this.gameObject.name + " is MOVING!");
        for (int i = 0; i < stepsToMove; i++)
        {
            var currentPos = this.transform.position;
            float newPosX = currentPos.x + DISTANTS_BETWEEEN_GRID;
            this.transform.position = new Vector3(newPosX, this.transform.position.y, this.transform.position.z);
            yield return new WaitForSeconds(WAIT_TIME_BETWEEN_STEPS);
        }
    }
}
