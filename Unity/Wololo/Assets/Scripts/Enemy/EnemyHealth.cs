﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : MonoBehaviour {

	public float maxHealth;
	public float curHealth;
    bool isHit;

	public GameObject healthBar;

	// Use this for initialization
	void Start () {

		curHealth = maxHealth;
		
	}


	//when the player gets damaged
	public void Damage () {
		//change 1f to the current damage of the player
		curHealth -= 1f;
		float calcHealth = curHealth / maxHealth;
		SetHealthBar (calcHealth);

	}

	public void SetHealthBar(float myHealth){
		healthBar.transform.localScale = new Vector3 (Mathf.Lerp (0f, 20.8f, myHealth), healthBar.transform.localScale.y, healthBar.transform.localScale.z);
	}
}
