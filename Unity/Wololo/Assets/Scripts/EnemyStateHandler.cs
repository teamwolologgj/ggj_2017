﻿using UnityEngine;

public class EnemyStateHandler : MonoBehaviour
{
    private IState m_State;
    private IState State
    {
        set
        {
            m_State = value;
            UpdateState();
        }
        get
        {
            return m_State;
        }
    }
    private SpriteRenderer _sprRenderer;
    [SerializeField]
    private Sprite[] _spriteSheet;
    private void Awake()
    {
        _sprRenderer = GetComponent<SpriteRenderer>();
    }
    private void Start()
    {
        Raid.OnRaidingStartEvent += Attack;
        PlayerAttack.OnDyingStartEvent += Death;

    }
    private void Attack(GameObject go)
    {
        if (go != this.gameObject)
            return;
        State = new StateAttacking();
    }

    private void Death(GameObject go)
    {
        if (go == this.gameObject)
            State = new StateDamaged();
    }

    private void UpdateState()
    {
        // Set sprite state
        _sprRenderer.sprite = _spriteSheet[(int)State.State];
    }

}
