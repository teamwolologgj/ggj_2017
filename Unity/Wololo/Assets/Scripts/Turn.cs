﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Turn : MonoBehaviour {


	public delegate void OnTurnEnd(bool isForced);
    public static event OnTurnEnd OnTurnEndEvent;

    public delegate void OnTurnStart();
    public static event OnTurnStart OnTurnStartEvent;

	//public Timer timer;
	private bool enemySelected;

    private void Start()
    {
        TurnStart();
		//timer.gameObject.SetActive (true);
    }
	public void TurnEnd(bool isForced)
    {
        Debug.Log("TurnEnd");
		TurnStart();
        if (OnTurnEndEvent == null)
            return;
		OnTurnEndEvent(isForced);


		
			//show a End Turn Banner of whatever

		/*
		 	Turn geforced gestopt, geef Feedback 
			Check voor GameOverState
		*/
    }
    private void TurnStart()
    {
        OnTurnStartEvent();
       // timer.gameObject.SetActive(true);
        /*	Move Enemies, 
            after they moved
            Timer.GameObject.SetActive("True");

            */
    }
	//Functie Bloop
	public void EnemySelected(/*enemy selected*/){
		
		enemySelected = true;
	
	}


}
