﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttack : MonoBehaviour
{

    CalibrateInput soundLevel;

    public delegate void OnDyingStart(GameObject dyingObject);
    public static event OnDyingStart OnDyingStartEvent;

    public GameObject SoundWaves;

    public void Hit()
    {
        this.SoundWaves.SetActive(true);
        Invoke("StopWaves", 2);

        if (EnemySelect.selectedEnemy)
        {

            //var enemyHealth = EnemySelect.selectedEnemy.GetComponent<EnemyHealth> ();
            var enemyStats = EnemySelect.selectedEnemy.GetComponent<Stats>();
            var enemyKnockback = EnemySelect.selectedEnemy.GetComponent<Knockback>();

            if (enemyStats.curHealth > 0)
            {

                enemyStats.curHealth -= PlayerStats.damage;
                if (enemyKnockback)
                    enemyKnockback.MoveToNewPosition();
            }
            else
            {
                Dying();
            }
        }

    }

    public void Dying()
    {
        if (OnDyingStartEvent != null)
            OnDyingStartEvent(EnemySelect.selectedEnemy);
    }

    private void StopWaves()
    {
        this.SoundWaves.SetActive(false);
    }
}
