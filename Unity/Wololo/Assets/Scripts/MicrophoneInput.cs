﻿using System;
using System.Collections;
using UnityEngine;
[RequireComponent(typeof(AudioSource))]
public class MicrophoneInput : MonoBehaviour
{
    public delegate void OnRecordingEnd(AudioSource recording);
    public static event OnRecordingEnd OnRecordEndEvent;

    private string _mic;
    private int _minFreq;
    private int _maxFreq;
    private AudioSource _audioSource;
    /// <summary>
    /// Time to record input in seconds
    /// </summary>
    private int _recordLength = 1;
    private void Awake()
    {
        _audioSource = GetComponent<AudioSource>();
        InitializeMic();

    }
    private void InitializeMic()
    {
        var mics = Microphone.devices;
        if (mics.Length == 0)
        {
            var excep = new System.Exception("Microphone not found Exception");
            Debug.LogException(excep);
            throw excep;
        }
        _mic = mics[0];
        // Debug input devices
        foreach (string mic in mics)
            Debug.Log(mic);
        // Get microphone stats
        Microphone.GetDeviceCaps(_mic, out _minFreq, out _maxFreq);
    }
    public void StartRecording()
    {
        _audioSource.clip = Microphone.Start(_mic, false, _recordLength, _maxFreq);
        StartCoroutine(CheckIfRecording());
    }
    /// <summary>
    /// Stops recording
    /// </summary>
    private void StopRecording()
    {
        Microphone.End(_mic);
    }
    private IEnumerator CheckIfRecording()
    {
        bool recording = Microphone.IsRecording(_mic);
        while (recording)
        {
            recording = Microphone.IsRecording(_mic);
            yield return new WaitForFixedUpdate();
        }
        OnRecordEndEvent(_audioSource);
    }
}
