﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NewBehaviourScript : MonoBehaviour {

    public List<GameObject> enemylist = new List<GameObject>();
    Scene currentScene;
    // Use this for initialization
    void Start () {
        currentScene = SceneManager.GetActiveScene();
        GetAllEnemy();
	}
	
	// Update is called once per frame
	void Update () {
        if (areAllDead())
        {
            if (currentScene.name == "Level2")
            {
                GameObject.Find("Replay").SetActive(true);
            }
            else
            {
                SceneManager.LoadScene("Shop");
            }
        }

	}

    public void GetAllEnemy()
    {     
        if (GameObject.FindGameObjectsWithTag("Enemy") != null)
        {
            enemylist.AddRange(GameObject.FindGameObjectsWithTag("Enemy"));
        }
    }

    bool areAllDead()
    {
        int count = 0;
        for (int i = 0; i < enemylist.Count; i++)
        {
            GameObject enemies = enemylist[i];
            if (enemies.gameObject.GetComponent<Stats>().health <= 0)
            {
                count++;
            }
        }
        if (count == enemylist.Count)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
