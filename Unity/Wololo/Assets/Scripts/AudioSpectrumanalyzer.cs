﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;

public class AudioSpectrumanalyzer : MonoBehaviour
{
    private static int GetSpectrumAudioSource(AudioSource audioSource)
    {
        float[] spectrum = new float[8192];
        audioSource.GetSpectrumData(spectrum, 0, FFTWindow.BlackmanHarris);

        float highestFreqValue = 0;
        int indexOfHighestFreq = 0;

        for (int i = 0; i < spectrum.Length; i++)
        {
            if (spectrum[i] > highestFreqValue)
            {
                highestFreqValue = spectrum[i];
                indexOfHighestFreq = i;
            }
        }
        return indexOfHighestFreq;
    }
    public static IEnumerator CalculateMedianFreq(AudioSource source,  Action<int> result)
    {
        int samplingTimes = (int)source.clip.length * 30;
        float timeStamp = 0;
        List<int> freqValues = new List<int>();
        float oneThirtieth = (1f / 30f);
        for (int i = 0; i < samplingTimes; i++)
        {
            //source.mute = true;
            if (i != 0)
                timeStamp += oneThirtieth;
            if (timeStamp > source.clip.length)
                break;
            source.time = timeStamp;
            source.Play();
            freqValues.Add(GetSpectrumAudioSource(source));
            yield return new WaitForFixedUpdate();
            source.Stop();
        }
        // Find Median from values
        freqValues = freqValues.OrderBy(x => x).ToList();
        int medianValue = freqValues[(int)freqValues.Count / 2];
        Debug.Log(medianValue);
        result(medianValue);
    }
}
