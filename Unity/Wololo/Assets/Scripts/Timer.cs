﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Timer : MonoBehaviour {

	public float maxTime;
	public float curTime;

	public Turn turn;

	public GameObject timeBar;

	void Start(){

        if (!turn)
            turn = FindObjectOfType<Turn>();
		this.gameObject.SetActive(false);
        Turn.OnTurnEndEvent += ResetTimer;

	}

    private void OnDestroy()
    {
        Turn.OnTurnEndEvent -= ResetTimer;
    }

	void OnEnable () {

		//float calcTime = curTime / maxTime;
		//SetTimeBar (calcTime);
		curTime = maxTime;
		InvokeRepeating ("CountDown", 0f, 0.01f);

	}
    private void ResetTimer(bool prima)
    {
        curTime = maxTime;
    }

	void Update(){

        if (!this.gameObject.activeInHierarchy)
            return;
		//Heurkenfleurp
		//float calcTime = curTime / maxTime;
		//SetTimeBar (calcTime);
		if (curTime <= 0) {
			turn.TurnEnd (true);
			this.gameObject.SetActive(false);
			}
		//if (){
		// if space is pressed stop the InvokeRepeating	
		//}

	}

	public void Stop(){

		CancelInvoke ();

	}

	public void  CountDown() {
		
		curTime -= 1f;
		float calcTime = curTime / maxTime;
		SetTimeBar (calcTime);

	}

	public void SetTimeBar(float myTime){
		timeBar.transform.localScale = new Vector3 (Mathf.Lerp (0f, 65f, myTime), timeBar.transform.localScale.y, timeBar.transform.localScale.z);
	}
}
