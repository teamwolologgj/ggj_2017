﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Stats), typeof(SpriteRenderer))]
public class Knockback : MonoBehaviour
{
    private GameObject particles;
    private SpriteRenderer _spr;
    private int FLICKER_TIMES = 2;
    private float FLICKER_TIME_MULTIPLIER = 1.35f;
    private Stats _stats;
    private void Awake()
    {
        _spr = GetComponent<SpriteRenderer>();
        _stats = GetComponent<Stats>();
    }
    public void MoveToNewPosition(Vector3 newPosition)
    {
        StartCoroutine(Flicker(newPosition));
    }
    public void MoveToNewPosition()
    {
        MoveToNewPosition(new Vector3(this.transform.position.x - EnemyMovement.DISTANTS_BETWEEEN_GRID*_stats.knockbackAmount, this.transform.position.y, this.transform.position.z));
    }
    private IEnumerator Flicker(Vector3 newPosition)
    {
        for (int i = 0; i < FLICKER_TIMES; i++)
        {
            while (_spr.color.a > 0.25)
            {
                var newColor = _spr.color;
                newColor.a -= Time.deltaTime * FLICKER_TIME_MULTIPLIER;
                _spr.color = newColor;
                yield return new WaitForFixedUpdate();
            }
            while (_spr.color.a < 1)
            {
                var newColor = _spr.color;
                newColor.a += Time.deltaTime * FLICKER_TIME_MULTIPLIER;
                _spr.color = newColor;
                yield return new WaitForFixedUpdate();
            }
        }
        PuffAnimation();
        // Wait just a little bit for the puff to play
        yield return new WaitForSeconds(0.25f);
        this.transform.position = newPosition;
    }
    private void PuffAnimation()
    {
        particles = GameObject.Instantiate((GameObject)Resources.Load("DustPartSystem"));
        this.particles.transform.position = this.transform.position;
        Invoke("EndDust", 2);
    }

    private void EndDust()
    {
        Destroy(this.particles);
    }
}
