﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateAttacking : IState
{
    private State m_State = State.attacking;
    public State State
    {
        get
        {
            return m_State;
        }
    }
}
