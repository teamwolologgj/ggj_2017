﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateIdle : IState
{
    private State m_State = State.idle;
    public State State
    {
        get
        {
            return m_State;
        }
    }
}
