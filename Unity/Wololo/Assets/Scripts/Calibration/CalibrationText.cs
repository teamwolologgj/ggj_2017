﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class CalibrationText : MonoBehaviour
{
    private Text text;

	public const string StartMessage = "Start whistling just before\nyou start the recording.\nPress space to get started!";
    public const string LowMessage = "Please whistle as low as you possibly can.";
    public const string MediumMessage = "Please whistle as high as you possibly can.";
    public const string HighMessage = "Please whistle at a pitch somewhere in between the previous two.";
    public const string EndMessage = "Press spacebar continue.";

    private static string[] messages = new string[] { StartMessage, LowMessage, MediumMessage, HighMessage, EndMessage };

	void Start()
    {
        this.text = GetComponent<Text>();
		this.text.text = StartMessage;

    }
	
    public void UpdateText(int state)
    {
        this.UpdateText(messages[state]);
    }

    public void UpdateText(string text)
    {
        this.text.text = text;
    }
}
