﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CalibrateInput : MonoBehaviour
{
    public enum SoundLevel
    {
        low,
        medium,
        high,
        invalid
    }


	private RecordFailed error;
	private SpeechTimer speech;
	private int k = 0;
    private bool isReady = true;
    private bool isDisposed = false;
    private CalibrationText CalibrationText;
    private List<Tuple<AudioSource, int>> _inputSounds = new List<Tuple<AudioSource, int>>();
    private List<AudioClip> _clips = new List<AudioClip>();
    private MicrophoneInput _micInput;
    private int _soundsCalibrated = 0;
    private int _resultValue = -1;
    private static bool isSorted = false;
    private void Start()
    {
        DontDestroyOnLoad(this.gameObject);
    }
    private void OnEnable()
    {
        _micInput = FindObjectOfType<MicrophoneInput>();
        MicrophoneInput.OnRecordEndEvent += SaveSound;
        CalibrationText = FindObjectOfType<CalibrationText>();
        speech = FindObjectOfType<SpeechTimer>();
        error = FindObjectOfType<RecordFailed>();
    }
    private void OnLevelWasLoaded()
    {
        Debug.Log("OnEnable");
        // Refit list
        for (int i = 0; i < _inputSounds.Count; i++)
        {
            var newSource = this.gameObject.AddComponent<AudioSource>();
            if (_clips.Count > i)
                newSource.clip = _clips[i];
            _inputSounds[i] = new Tuple<AudioSource,int>(newSource, _inputSounds[i].Second);
        }
        _micInput = FindObjectOfType<MicrophoneInput>();
        MicrophoneInput.OnRecordEndEvent += SaveSound;
        CalibrationText = FindObjectOfType<CalibrationText>();
        speech = FindObjectOfType<SpeechTimer>();
        error = FindObjectOfType<RecordFailed>();
    }
    void Update()
    {
        if (!this.isDisposed)
        {
            if (this.isReady && Input.GetKeyDown(KeyCode.Space) && !isSorted)
            {
                Debug.Log("Calibrate sound input");
                this.UpdateText(false);

				if (_soundsCalibrated < 3 && k > 0) {
					StartCalibration ();
					//UpdateText ();
				} 
				else if(_soundsCalibrated < 3 && k == 0)
				{
					k++;
					UpdateText (true);
				}
                else
                    SortCalibration();
            }
        }
    }

    private void UpdateText(bool b)
    {
       // CalibrationText text = this.CalibrationText.GetComponent<CalibrationText>();
        if (CalibrationText != null)
        {
            if (b)
            {

                //CalibrationText.UpdateText(this._soundsCalibrated);
				CalibrationText.UpdateText(this.k);
            }
            else
                CalibrationText.UpdateText("");
                //CalibrationText.UpdateText("bloop");
        }
    }

    public AudioSource GetCalibratedSound(SoundLevel soundLevel)
    {
        return _inputSounds[(int)soundLevel].First;
    }
    public SoundLevel FindSoundLevel(int soundValue)
    {
        if (_inputSounds.Count < 3)
        {
            Debug.LogError("Sound calibration not completed");
            return SoundLevel.invalid;
        }
        if (soundValue == 0)
            return SoundLevel.invalid;
        var soundValuePower = Math.Log(soundValue, 2);
        // Global Game Jam Cooooddeee~~~~~~~~~
        var lowSoundLevelPower = Math.Log(_inputSounds[(int)SoundLevel.low].Second, 2);
        var mediumSoundLevelPower = Math.Log(_inputSounds[(int)SoundLevel.medium].Second, 2);
        var diffLowMedium = mediumSoundLevelPower - lowSoundLevelPower;
        var highSoundLevelPower = Math.Log(_inputSounds[(int)SoundLevel.high].Second, 2);
        var diffMedHigh = highSoundLevelPower - mediumSoundLevelPower;
        if (soundValuePower < lowSoundLevelPower)
            return SoundLevel.low;
        else if (soundValuePower < mediumSoundLevelPower && soundValuePower < lowSoundLevelPower + diffLowMedium / 2)
            return SoundLevel.low;
        else if (soundValuePower < mediumSoundLevelPower)
            return SoundLevel.medium;
        else if (soundValuePower < highSoundLevelPower && soundValuePower < mediumSoundLevelPower + diffMedHigh / 2)
            return SoundLevel.medium;
        else
            return SoundLevel.high;

    }
    public void StartCalibration()
    {
        _resultValue = -1;
        _micInput.StartRecording();
		speech.gameObject.SetActive (true);
    }
    private void SaveSound(AudioSource source)
    {
        var Scene = SceneManager.GetActiveScene();
        if (Scene.name.Equals("Level1") || (Scene.name == "Shop") || (Scene.name == "Level2"))
            return;
        var newSource = this.gameObject.AddComponent<AudioSource>();
        newSource.clip = source.clip;
        StartCoroutine(AudioSpectrumanalyzer.CalculateMedianFreq(source, (x) =>
        {
            if (x < 70)
            {
                Debug.Log("Try again feedback Text!"); // Activate
					error.gameObject.SetActive(true);
                StopAllCoroutines();
                this.isReady = true;
                Destroy(newSource);
            }
            else
                _resultValue = x;
        }
        ));
        StartCoroutine(WaitForResult(source));

    }
    private IEnumerator WaitForResult(AudioSource source)
    {
        while (_resultValue == -1)
        {
            yield return new WaitForFixedUpdate();
        }
        _inputSounds.Add(new Tuple<AudioSource, int>(source, _resultValue));
        _soundsCalibrated++;
		k++;
        // Delay and animations required

        this.UpdateText(true);

        this.isReady = true;
    }
    private void SortCalibration()
    {
        List<Tuple<AudioSource, int>> tempList = new List<Tuple<AudioSource, int>>();
        int lowestValue = 0;
        int indexOfLowestValue = 0;
        while (tempList.Count < _inputSounds.Count)
        {
            for (int i = 0; i < _inputSounds.Count; i++)
            {
                if (_inputSounds[i].Second < lowestValue)
                {
                    lowestValue = _inputSounds[i].Second;
                    indexOfLowestValue = i;
                }
            }
            tempList.Add(_inputSounds[indexOfLowestValue]);
        }
        _inputSounds.Clear();
        foreach (var element in tempList)
        {
            _inputSounds.Add(element);
            RefSave.inputSounds.Add(element);
            Debug.Log(element.First);
            _clips.Add(element.First.clip);
        }
        Debug.Log(_inputSounds[0]);
        this.isDisposed = true;
        isSorted = true;
        SceneManager.LoadScene("Level1");
    }
}
