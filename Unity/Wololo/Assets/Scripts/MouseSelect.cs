﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseSelect : MonoBehaviour {

	void Update () {
        if (Input.GetMouseButtonDown(0))
        {
            var rayCastResults = Physics2D.RaycastAll(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.up, 1);
            foreach (var result in rayCastResults)
            {
                if (result.collider)
                {
                    if (result.collider.tag == "Enemy")
                    {
                        if (result.collider.GetComponent<EnemySelect>())
                        {
                            result.collider.GetComponent<EnemySelect>().Select();
                        }
                        else
                        {
                            Debug.LogError("EnemySelect Script is not found on enemy: " + result.collider.gameObject.name);
                        }
                    }

                    if (result.collider.tag == "Item")
                    {
                        if (result.collider.GetComponent<ShopSelect>())
                        {
                            result.collider.GetComponent<ShopSelect>().Select();
                        }
                    }
                }
            }
        }
    }
}
