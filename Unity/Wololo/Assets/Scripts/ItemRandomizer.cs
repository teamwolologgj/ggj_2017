﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemRandomizer : MonoBehaviour {

    Vector3 item;
    float moveSpot;
    float startPos;
    public List<GameObject> itemlist = new List<GameObject>();

    // Use this for initialization
    void Awake () {
        startPos = -4.5f;
        GetAllItems();
        RandomizeList();
        GenerateLocations();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void GetAllItems()
    {
        if (GameObject.FindGameObjectsWithTag("Item") != null)
        {
            itemlist.AddRange(GameObject.FindGameObjectsWithTag("Item"));
        }
    }
    
    void RandomizeList()
    {
        for (int i = 0; i < itemlist.Count; i++)
        {
            GameObject temp = itemlist[i];
            int randomIndex = Random.Range(i, itemlist.Count);
            itemlist[i] = itemlist[randomIndex];
            itemlist[randomIndex] = temp;
        }
    }


    void GenerateLocations()
    {
        for (int i = 0; i < itemlist.Count; i++)
        {
            GameObject items = itemlist[i];
            items.transform.position = new Vector3(startPos + moveSpot, 0, 1);
            moveSpot += 4.5f;
        }
    }
}
