﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeechTimer : MonoBehaviour {

	public float maxTime;
	public float curTime;

	public bool startActive = false;

	public GameObject timeBar;

	void Start(){

		this.gameObject.SetActive(startActive);

	}

	void OnEnable () {

		//float calcTime = curTime / maxTime;
		//SetTimeBar (calcTime);
		curTime = maxTime;
		InvokeRepeating ("CountDown", 0f, 0.01f);

	}

	void Update(){

		//Heurkenfleurp
		//float calcTime = curTime / maxTime;
		//SetTimeBar (calcTime);
		if (curTime <= 0) {
            CancelInvoke();
            this.gameObject.SetActive(false);
		}

	}


	public void  CountDown() {

		curTime -= 1f;
		float calcTime = curTime / maxTime;
		SetTimeBar (calcTime);

	}

	public void SetTimeBar(float myTime){
		timeBar.transform.localScale = new Vector3 (Mathf.Lerp (0f, 40f, myTime), timeBar.transform.localScale.y, timeBar.transform.localScale.z);
	}
}
