﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Raid : MonoBehaviour {
    
    bool isHit;
    public delegate void OnRaidingStart(GameObject go);
    public static event OnRaidingStart OnRaidingStartEvent;

    private void Start()
    {
        isHit = false;
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        Debug.Log("RAIDING PARTY");
        OnRaidingStartEvent(col.gameObject);
        PlayerStats.health--;
        if (!isHit)
        {
            if (PlayerStats.health <= 0)
            {
                Debug.Log("Game Over!!!!!!!");
            }
            isHit = true;
        }
        
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        isHit = false;
    }

}
